from flask import Flask, render_template, request, jsonify
from recastai import Connect
import requests
import recastai
from flask_bootstrap import Bootstrap 
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired

class MyForm(FlaskForm):
    post_message = TextAreaField('Type your message: ', validators=[DataRequired()])

req_token = '76694a0361f99ae45cb29f69d0fff6a5'
dev_token = 'b0cce1aa1e67e38842d266220442fb43'
conv_url = 'https://api.recast.ai/connect/v1/conversations/'

connect = Connect(req_token)
build = recastai.Build(req_token, 'en')

conv_id = "fb26d0a8-b302-41e6-b305-bcbe4ffe3414"

human_id = "" 
bot_id = ""

history = []

skilled_messages = []
unskilled_messages = []
conv = {}
def get_detailed_message(request): 
	message = connect.parse_message(request)
	global conv_id
	conv_id = message.conversation_id
	print(conv)
	message = message.content
	curr = {}
	if message["intents"] == []: 
		curr["content"] = message["source"]
		unskilled_messages.append(curr)
	else: 
		curr["content"] = message["source"]
		curr["intent"] = message["intents"][0]["slug"]
		curr["confidence"] = message["intents"][0]["confidence"]
		curr["sentiment"] = message["sentiment"]
		skilled_messages.append(curr)
	return jsonify(status= 200)

def get_participants_ids(): 

	participants = conv["results"]["participants"]

	for participant in participants:
		if(participant['isBot'] == False):
			hid = participant['id']
		else: 
			bid = participant['id'] 

	return hid, bid



def get_conversation_history(human_id, bot_id): 
	
	messages = conv["results"]["messages"]
	hist = []
	for message in messages:
		curr = {}
		if message['participant'] == human_id: 
			curr["sender"] = "Human"
		elif message['participant'] == bot_id: 
			curr["sender"] = "Bot"
		curr["content"] = message['attachment']['content']
		hist.append(curr)

	return hist	



app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config['SECRET_KEY'] = "Don't tell anyone"
csrf = CSRFProtect(app)


@app.route('/', methods=['POST', 'GET'])
@csrf.exempt
def index(): 
	url = conv_url + conv_id
	if request.method == 'POST': 
		message_to_post = request.form['post_message']
		response = requests.post(url + '/messages', json={'messages': [{'type': 'text', 'content': "{}".format(message_to_post)}]}, headers={'Authorization': "Token {}".format(dev_token)})
		print(response.text)

	global conv, human_id, bot_id, history
	conv = requests.get(url, headers={'Authorization': "Token {}".format(dev_token)}).json()
	print(conv)
	if(conv["results"] != None): 
		human_id, bot_id = get_participants_ids()
		history = get_conversation_history(human_id, bot_id)
	
	global form 
	form = MyForm()

	return render_template('index.html', form= form, human_id= human_id, bot_id= bot_id, history= history, skilled_messages= skilled_messages, unskilled_messages= unskilled_messages)

@csrf.exempt
@app.route('/update', methods=['POST'])
def message_update():
	return get_detailed_message(request)

if __name__ == '__main__': 
	app.run(port = 5050, debug=True) 
